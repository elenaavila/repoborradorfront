import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

import '@polymer/polymer/lib/elements/dom-repeat.js';

import '@vaadin/vaadin-grid/vaadin-grid.js';
import '@vaadin/vaadin-grid/vaadin-grid-sort-column.js';

/**
 * @customElement
 * @polymer
 */
class VisorCuenta extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
        .redbg{
          background-color: red;
        }
        .greenbg{
          background-color: green;
        }
        .bluebg{
          background-color: blue;
        }
        .greybg{
          background-color: grey;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


      <!--  <div hidden$="{{!hasAccounts}}">
          <div class="row bluebg">
            <div class="col-4">IBAN</div>
            <div class="col-3">BALANCE</div>
            <div class="col-2">DIVISA</div>
          </div>
        </div>
      <div class="row greenbg">
        <div class="col-1">XX</div>
        <div class="col-3">[[iban]]</div>
        <div class="col-3">[[balance]]</div>
        <div class="col-2">[[currency]]</div>
      </div>
      <dom-repeat items="[[accounts]]">
        <template>
        <div class="row redbg">
          <div class="col-1">[[index]]</div>
          <div class="col-3">[[item.iban]]</div>
          <div class="col-3">[[item.balance]]</div>
          <div class="col-2">[[item.currency]]</div>
        </div>
        </template>
      </dom-repeat>-->
      <div hidden$="{{!hasAccounts}}">
        <h2 >LISTADO CUENTAS DEL USUARIO [[user_id]]</h2>
        <vaadin-grid items="[[accounts]]">
          <vaadin-grid-column header="#"><template>[[index]]</template></vaadin-grid-column>
          <vaadin-grid-column header="IBAN"><template>[[item.iban]]</template></vaadin-grid-column>
          <vaadin-grid-sort-column path="balance" header="BALANCE" direction="desc"></vaadin-grid-sort-column>
          <vaadin-grid-column path="currency" header="DIVISA" text-align="end"></vaadin-grid-column>
        </vaadin-grid>
      </div>
      <div hidden$="{{!noAccounts}}">
        <h3>No tienes cuentas asociadas</h3>
      </div>
      <iron-ajax
        id="getAccounts"
        url="http://localhost:3000/apitechu/v2/accounts/?$userId={{user_id}}"
        handle-as="json"
        on-response="showData"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      user_id: {
        type: Number,
        observer: "_userIdChanged"
      },
      iban: {
        type: String
      },
      balance: {
        type: Number
      },
      currency: {
        type: String
      },
      accounts: {
        type: Array
      },
      hasAccounts:{
          type: Boolean,
          value: false
      },
      noAccounts:{
          type: Boolean,
          value: false
      }
    };
  }//End properties

  showData(data){
    console.log("showData account");
    console.log(data);
    console.log(data.detail.response);
  //  console.log(data.detail.response[0].iban);
  //  this.user_id = data.detail.response[0].user_id;
  //  this.iban = data.detail.response[0].iban;
  //  this.balance = data.detail.response[0].balance;
  //  this.currency = data.detail.response[0].currency;
    this.accounts = data.detail.response;
  //  this.push('accounts', data.detail.response);
    if(this.accounts.length > 0){
      console.log("showData tiene cuentas el usuario:"+this.user_id);
      this.hasAccounts=true;
      this.noAccounts=false;
    }else{
      this.hasAccounts=false;
      this.noAccounts=true;
      console.log("showData NO tiene cuentas el usuario:"+this.user_id);
    }
  }

  _userIdChanged(newValue, oldValue){
    console.log("visor cuenta _userIdChanged.-Id usuario value has changed");
    console.log("Old value was "+oldValue);
    console.log("New value is "+newValue);

    console.log("Invocacion ajax a getUserById");
    //Funcion iron-ajax para enviar la peticion
    this.$.getAccounts.generateRequest();
  }

}//End classr { SyntaxError: Unexpected token (75:

window.customElements.define('visor-cuenta', VisorCuenta);
