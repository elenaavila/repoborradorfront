import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

import '../visor-cuenta/visor-cuenta.js';

/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          /*ESTO HACE QUE NO SE PROPAGUEN LOS ESTILOS DEL PADRE(GLOBALES) all:initial;*/
        /*  border:solid blue;*/
        }
        .redbg{
          background-color: red;
        }
        .greenbg{
          background-color: green;
        }
        .bluebg{
          background-color: blue;
        }
        .greybg{
          background-color: grey;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!--  <div class="row greybg">
        <div class="col-2 col-sm-6 redbg">Col 1</div>
        <div class="col-3 col-sm-1 greenbg">Col 2</div>
        <div class="col-4 col-sm-1 bluebg">Col 3</div>
      </div>
      <div class="row greybg">
        <div class="col-2 offset-1 redbg">Col 1</div>
        <div class="col-3 offset-2 greenbg">Col 2</div>
        <div class="col-4 bluebg">Col 3</div>
      </div>
      <button class="btn btn-info">Login</button>
      <button class="btn btn-success">Logout</button>
      <button class="btn btn-danger">Login</button>
      <button class="btn btn-warning">Logout</button>
      <button class="btn btn-primary btn-lg">Login</button>
      <button class="btn btn-secondary btn-sm">Logout</button>
      <button class="btn btn-light">Login</button>
      <button class="btn btn-dark">Logout</button>-->
      <div hidden$="{{!isLogged}}">
        <h2>Soy [[first_name]] [[last_name]]</h2>
        <h2>y mi email es [[email]]</h2>
        <br/>
        <button class="btn btn-dark" on-click="verCuentas">Ver cuentas</button>
        <br/><br/>
        <visor-cuenta id="receiverUserId"></visor-cuenta>
      </div>
      <iron-ajax
        id="getUser"
        url="http://localhost:3000/apitechu/v2/users/{{id_user}}"
        handle-as="json"
        on-response="showData"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      id_user: {
        type: Number,
        observer: "_userIdChanged"
      },
      first_name: {
        type: String
      },
      last_name: {
        type: String
      },
      email: {
        type: String
      },
      isLogged:{
        type: Boolean,
        value: false
      }
    };
  }//End properties

  showData(data){
    console.log("showData");
    console.log(data);
    console.log(data.detail.response);
    console.log(data.detail.response.first_name);
    this.first_name = data.detail.response.first_name;
    this.last_name = data.detail.response.last_name;
    this.email = data.detail.response.email;
    this.isLogged=true;
    this.$.receiverUserId.hasAccounts = false;
    this.$.receiverUserId.noAccounts = false;
  }

  _userIdChanged(newValue, oldValue){
    console.log("_userIdChanged.-Id usuario value has changed");
    console.log("Old value was "+oldValue);
    console.log("New value is "+newValue);

    console.log("Invocacion ajax a getUserById");
    //Funcion iron-ajax para enviar la peticion
    this.$.getUser.generateRequest();
  }

  verCuentas(){
    console.log("Funcion verCuentas en visor-usuario. UserID:"+this.id_user);
    this.$.receiverUserId.user_id = this.id_user;

  }

}//End class

window.customElements.define('visor-usuario', VisorUsuario);
