import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

import '../login-usuario/login-usuario.js';
import '../visor-usuario/visor-usuario.js';

/**
 * @customElement
 * @polymer
 */
class UserDashboard extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h1>Soy el USER DASHBOARD</h1>
      <login-usuario on-loginsuccess="processEvent"></login-usuario>

      <visor-usuario id="receiver"></visor-usuario>
    `;
  }
  static get properties() {
    return {

    };
  }

  processEvent(e){
    console.log("Funcion processEvent en USER DASHBOARD. Capturado evento (loginsuccess) del login-usuario");
    console.log(e);
    this.$.receiver.id_user = e.detail.id_user;
  }

}

window.customElements.define('user-dashboard', UserDashboard);
