import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <h2>Soy el emisor de evento. LOGIN</h2>
      <input type="email" id="mail" placeholder="Email" required value="{{email::input}}"/>
      <br/>  <br/>
      <input type="password" placeholder="Contraseña" required value="{{password::input}}"/>
      <br/>  <br/>

      <button class="btn btn-success" on-click="login">Login</button>
      <br/> <br/>
      <h3 hidden$="{{!isLogged}}">Bienvenid@ de nuevo!! Usuario [[id_user]]</h3>

      <iron-ajax
        id="doLogin"
        url="http://localhost:3000/apitechu/v2/login"
        handle-as="json"
        content-type="application/json"
        method="POST"
        on-response="manageAJAXResponse"
        on-error="showError"
      >
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      email: {
        type: String
      },
      password: {
        type: String
      },
      id_user: {
        type: Number
      },
      isLogged:{
        type: Boolean,
        value: false
      }
    };
  }

  login(e){
    console.log("Funcion login.- El usuario ha pulsado el boton login");

    var loginData = {
      "email": this.email,
      "password": this.password
    }

    //console.log(loginData);
    this.$.doLogin.body = JSON.stringify(loginData);
    //Funcion iron-ajax para enviar la peticion
    this.$.doLogin.generateRequest();

  }

  manageAJAXResponse(data){
      console.log("manageAJAXResponse.- Llegaron los datos de respuesta");
      console.log(data.detail.response);

      this.isLogged=true;
      this.id_user =  data.detail.response.idUsuario;
      //Creamos nuevo evento para visor-usuario
      this.dispatchEvent(
        new CustomEvent(
          "loginsuccess",
          {
            "detail": {
              "id_user": data.detail.response.idUsuario
            }
          }
        )
      );
  }

  showError(error){
    console.log("Hubo un error");
    console.log(error);
    console.log(error.detail.request.xhr.response.mensaje);
    console.log(error.detail.request.xhr.status);
  }
}

window.customElements.define('login-usuario', LoginUsuario);
